from random import randint

print("What is your name?")
name = input()
print(f'Hello, {name}!')

calendar_months = ['Jan','Feb','Mar','Apr','May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec']

guesses = int(input("How many guesses are you giving me?"))

for guess in range(guesses):
    month = calendar_months[randint(1,12) - 1]
    year = randint(1924, 2004)
    print(f'{name}, were you born in {month} / {year}?')
    answer = input('Yes or No?')
    if answer == 'Yes' or answer == 'yes':
        print('I knew it!')
        break
    if guesses == 1:
        print('I have other things to do. Good bye.')
        break
    elif answer == 'No' or answer == 'no':
        guesses -= 1
        print('Drat! Lemme try again!')
